{-# LANGUAGE FlexibleContexts #-}

module Main where

import System.IO
import System.Directory
import System.Environment

import qualified Data.ByteString as B
import qualified Data.ByteString.UTF8 as U
import qualified Data.Map as M
import Control.Monad.State.Lazy
import Control.Monad.Reader
import Control.Monad.IO.Class

import Gaia.Engine as Gaia
import Gaia.IO.Resource
import Gaia.Debug.Log
import Gaia.Rendering.Types
import Gaia.Rendering.RenderStage
import Gaia.Rendering.Renderer
--import Gaia.Rendering.Pipeline

import Gaia.Rendering.GL
import Gaia.Rendering.GL.Shader

import Gaia.Rendering.Vulkan.Init

loadShaderSource :: String -> FilePath -> ResourceOpIO ShaderSource String--ResourceOp ShaderSource IO String
loadShaderSource rid fp = do
    r <- loadResourceFromFile rid fp shaderSourceLoader
    case r of 
        Nothing -> return ""
        Just r' -> return ( source r' )

resourceTest :: IO ()
resourceTest = do
    wd <- getCurrentDirectory
    ( fs, cache )  <- runResourceOp ( loadShaderSource "ObjectFS" ( wd ++ "//dataSource//effects//object//fs_object.fsh" ) ) mkCache
    ( vs, cache' ) <- runResourceOp ( loadShaderSource "ObjectVS" ( wd ++ "//dataSource//effects//object//vs_object.vsh" ) ) cache
    putStrLn fs
    putStrLn vs
    logMsg $ show ( getResourceIdList cache' )
    putStrLn "Done"

main :: IO ()
--main = resourceTest
main = getArgs >>= Gaia.execEngine runTests "GaiaTest"

runTests :: Scene
runTests = Scene $ do
    liftIO resourceTest
    vkInstance <- liftIO createInstance
    return ( mainLoop 0 )

mainLoop :: Int -> Scene

mainLoop 3 = Scene $ do
    engine <- get
    let et      = getEngineTimeInSeconds engine

    let val     = ( cos et ) * 0.5 + 0.5  
    consumeRenderStage RenderStage { 
        name = "Test", 
        prologue = defaultPrologue { clearColour = Just $ Colour4 val 0.0 val 1.0 }, 
        epilogue = defaultEpilogue, 
        stream = [] 
    }

    let dt      = getDeltaTimeInSeconds engine
    let maxTime = 60
    if et > maxTime then do
        logMsg "Time's up" 
        return ( tickerLoop 200 )
    else do 
        logMsg ( "Only have " ++ ( show (maxTime - et) ) ++ "s left - dt = " ++ ( show dt ) ) 
        return ( mainLoop 3 )

mainLoop n = Scene $ do
    let colours = [ Colour4 1.0 0.0 0.0 1.0, Colour4 0.0 1.0 0.0 1.0, Colour4 0.0 0.0 1.0 1.0 ]

    consumeRenderStage RenderStage { 
        name = "Test", 
        prologue = defaultPrologue { clearColour = Just $ ( colours !! n ) }, 
        epilogue = defaultEpilogue, 
        stream = [] 
    }
    --liftIO $ pipeM_ mkPipeline [RenderStage { name = "Test", prologue = p, epilogue = e, stream = [] }]

    engine <- get
    let et      = getEngineTimeInSeconds engine
    let dt      = getDeltaTimeInSeconds engine
    let maxTime = 10 * ( n + 1 )
    if et > ( fromIntegral maxTime ) then do
        return $ mainLoop ( n + 1 )
    else do 
        logMsg ( "Loop " ++ ( show n ) ++ " Only have " ++ ( show ( ( fromIntegral maxTime ) - et ) ) ++ "s left - dt = " ++ ( show dt ) ) 
        return $ mainLoop n

tickerLoop :: Integer -> Scene
tickerLoop 0 = Scene $ return Gaia.exitEngine
tickerLoop n = Scene $ do
    logMsg ( "Tick : " ++ ( show n ) )
    --liftIO $ putStrLn ( "Tick : " ++ ( show n ) )
    return ( tickerLoop ( n - 1 ) )