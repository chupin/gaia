module EngineConfig where

-- |A basic engine configuration
data EngineConfig = EngineConfig {
    ecDataSource :: !FilePath
}

-- |Create an engine config
defaultEngineConfig :: EngineConfig
defaultEngineConfig = EngineConfig {
    ecDataSource = "."
}
