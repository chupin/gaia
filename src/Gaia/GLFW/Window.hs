module Gaia.GLFW.Window (
    Window, 
    mkWindow, 
    destroyWindow, 
    setWindowShouldClose,
    windowSwap, 
    windowShouldClose,
    pollEvents,
    getApplicationTimeInSeconds,
    ) where

-- Globals
import Gaia.Debug.Log

-- Internals
import qualified "GLFW-b" Graphics.UI.GLFW as GLFW
import Data.Maybe

-- Basic window
data Window = Window { 
    wWindow :: !GLFW.Window 
}

-- Creates a window
mkWindow :: String -> Int -> Int -> IO (Maybe Window)
mkWindow defaultWindowName defaultWindowWidth defaultWindowHeight = do
    initStatus <- GLFW.init
    case initStatus of 
        False -> logError "Failed to initialise GLFW" >> GLFW.terminate >> return Nothing
        True  -> do     
            mWindow <- GLFW.createWindow defaultWindowWidth defaultWindowHeight defaultWindowName Nothing Nothing  
            case mWindow of 
                Nothing     -> logError "Failed to create GLFW window" >> GLFW.terminate >> return Nothing
                ( Just w )  -> do 
                        -- Window hints
                        mapM_ GLFW.windowHint defaultWindowHints

                        -- Callbacks
                        GLFW.setWindowPosCallback w ( Just onWindowMoved ) 
                        GLFW.setWindowSizeCallback w ( Just onWindowResized )  
                        GLFW.setWindowFocusCallback w ( Just onWindowFocusChanged )
                        GLFW.setMouseButtonCallback w ( Just onMouseClicked )
                        GLFW.setKeyCallback w ( Just onKey )

                        -- Make context current
                        GLFW.makeContextCurrent ( Just w )

                        -- Vsync when it works...
                        GLFW.swapInterval 1

                        -- Done, return the window                  
                        return $ Just Window { wWindow = w }
                where
                    defaultWindowHints  = [ 
                        GLFW.WindowHint'sRGBCapable True, 
                        GLFW.WindowHint'OpenGLDebugContext True, 
                        GLFW.WindowHint'OpenGLProfile GLFW.OpenGLProfile'Core,
                        GLFW.WindowHint'ContextVersionMajor 4,
                        GLFW.WindowHint'ContextVersionMajor 5
                        ]

-- |Requests the given window closes
setWindowShouldClose :: Window -> Bool -> IO()
setWindowShouldClose w b = GLFW.setWindowShouldClose ( wWindow w ) b

-- |Destroys the given window
destroyWindow :: Window -> IO()
destroyWindow w = do 
    GLFW.destroyWindow ( wWindow w ) 
    GLFW.terminate

-- |Swaps the window's buffers
windowSwap :: Window -> IO()
windowSwap w = GLFW.swapBuffers ( wWindow w )

-- |Poll events
pollEvents :: Window -> IO ()
pollEvents _ = GLFW.pollEvents

-- |Returns the application time /(in seconds)/
getApplicationTimeInSeconds :: IO Double
getApplicationTimeInSeconds = GLFW.getTime >>= return . fromMaybe 0.0 

-- Should the window close
windowShouldClose :: Window -> IO Bool
windowShouldClose w = GLFW.windowShouldClose ( wWindow w )

-- Mouse callback
onMouseClicked :: GLFW.MouseButtonCallback
onMouseClicked _ b s _ = logInfo $ "Mouse clicked : " ++ show b ++ " => " ++ show s

-- Keyboard callback
onKey :: GLFW.KeyCallback
onKey _ k i s _ = logInfo $ "Keyboard : " ++ show k ++ " " ++ show i ++ " => " ++ show s

-- Respond to window resizing
onWindowResized :: GLFW.WindowSizeCallback 
onWindowResized _ newWidth newHeight = logInfo $ "Window resized to " ++ show newWidth ++ "x" ++ show newHeight

-- Respond to window moving
onWindowMoved :: GLFW.WindowPosCallback
onWindowMoved _ newX newY = logInfo $ "Window moved to " ++ show newX ++ "x" ++ show newY

-- Respond to window focus changes
onWindowFocusChanged :: GLFW.WindowFocusCallback 
onWindowFocusChanged _ f = if f then logInfo "Gained focus..." else logInfo "Lost focus..."