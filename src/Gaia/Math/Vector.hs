module Gaia.Math.Vector (
    Vector(..), 
    -- | = Arithmetic
    append,
    dot, 
    add,
    sub,
    mul,
    rcpMul,
    magnitude,
    normalize
    ) 
where

import Prelude( Floating, Fractional, Num, Show(..), ($), (/), (*), (+), (++), sqrt, error )

-- |Basic data types for Vectors
data Vector a = Vector4 a a a a | Vector3 a a a | Vector2 a a

-- |Instance of Show for convenience
instance (Show a) => Show (Vector a) where
    show (Vector2 x0 y0)                            = "(" ++ show x0 ++ "," ++ show y0 ++ ")"
    show (Vector3 x0 y0 z0)                         = "(" ++ show x0 ++ "," ++ show y0 ++ "," ++ show z0 ++ ")"
    show (Vector4 x0 y0 z0 w0)                      = "(" ++ show x0 ++ "," ++ show y0 ++ "," ++ show z0 ++ "," ++ show w0 ++ ")"

-- |Returns the appended vector with a scalar /(useful to promote a 2D vector to 3D and so on)/
append :: ( Num a ) => Vector a -> a -> Vector a
append (Vector2 x0 y0) z    = Vector3 x0 y0 z
append (Vector3 x0 y0 z0) w = Vector4 x0 y0 z0 w
append _ _                  = error "Not supported"

-- |Returns the dot product of a vector
dot :: ( Num a ) => Vector a -> Vector a -> a
dot (Vector2 x0 y0) (Vector2 x1 y1)                 = ( x0 * x1 ) + ( y0 * y1 )
dot (Vector3 x0 y0 z0) (Vector3 x1 y1 z1)           = ( x0 * x1 ) + ( y0 * y1 ) + ( z0 * z1 )
dot (Vector4 x0 y0 z0 w0) (Vector4 x1 y1 z1 w1)     = ( x0 * x1 ) + ( y0 * y1 ) + ( z0 * z1 ) + ( w0 * w1 )
dot _ _                                             = error "Not supported"

-- |Returns the sum of two vectors
add :: ( Num a ) => Vector a -> Vector a -> Vector a 
add (Vector2 x0 y0) (Vector2 x1 y1)                 = Vector2 ( x0 + x1 ) ( y0 + y1 )
add (Vector3 x0 y0 z0) (Vector3 x1 y1 z1)           = Vector3 ( x0 + x1 ) ( y0 + y1 ) ( z0 + z1 )
add (Vector4 x0 y0 z0 w0) (Vector4 x1 y1 z1 w1)     = Vector4 ( x0 + x1 ) ( y0 + y1 ) ( z0 + z1 ) ( w0 + w1 )
add _ _                                             = error "Not supported"

-- |Returns the subtraction of two vectors
sub :: ( Num a ) => Vector a -> Vector a -> Vector a 
sub v0 v1 = add v0 ( mul v1 (-1) ) 

-- |Returns the multiplication of a vector and a scalar
mul :: ( Num a ) => Vector a -> a -> Vector a 
mul (Vector2 x0 y0) s       = Vector2 ( x0 * s ) ( y0 * s )
mul (Vector3 x0 y0 z0) s    = Vector3 ( x0 * s ) ( y0 * s ) ( z0 * s )
mul (Vector4 x0 y0 z0 w0) s = Vector4 ( x0 * s ) ( y0 * s ) ( z0 * s ) ( w0 * s )

-- |Returns the reciproqual multiplication (i.e. division) of a vector and a scalar
rcpMul :: ( Fractional a ) => Vector a -> a -> Vector a 
rcpMul v d = mul v ( 1.0 / d )

-- |Returns the magnitude of a vector
magnitude :: ( Floating a ) => Vector a -> a
magnitude v = sqrt $ dot v v

-- |Normalizes the given vector
normalize :: ( Floating a ) => Vector a -> Vector a
normalize v = v `rcpMul` ( magnitude v )




