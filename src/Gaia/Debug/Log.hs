module Gaia.Debug.Log (
    Logger(..),
    -- | = Basic printing
    printError,
    printWarning,
    printSuccess,
    printInfo,
    printMsg
    ) 
where

-- Internals
import System.IO
import System.Console.ANSI
import Data.Time.Clock
import Data.Time.Format
import Control.Monad.IO.Class

{-# ANN module "HLint: ignore Use putStrLn" #-}

-- |Returns the current formatted time
getCurrentTimeFormatted :: IO String
getCurrentTimeFormatted = do
    currentTime <- getCurrentTime
    return $ (formatTime defaultTimeLocale "%T") currentTime

-- |Prints an error message /(prefixed with the '__[ERROR]__' tag)/
printError :: String -> IO ()
printError s = do 
    formattedTime <- getCurrentTimeFormatted
    hSetSGR stderr [SetColor Foreground Vivid Red]
    hPutStrLn stderr ( "[" ++ formattedTime ++ "] " ++ "[ERROR] " ++ s ) 
    hSetSGR stderr [Reset]

-- |Prints a warning message /(prefix with the '__[WARNING]__' tag)/
printWarning :: String -> IO ()
printWarning s = do
    formattedTime <- getCurrentTimeFormatted
    hSetSGR stdout [SetColor Foreground Vivid Yellow]
    hPutStrLn stdout ( "[" ++ formattedTime ++ "] " ++ "[WARNING] " ++ s ) 
    hSetSGR stdout [Reset]

-- |Prints a success message /(prefixed with the '__[SUCCESS]__' tag)/
printSuccess :: String -> IO ()
printSuccess s = do
    formattedTime <- getCurrentTimeFormatted
    hSetSGR stdout [SetColor Foreground Vivid Green]
    hPutStrLn stdout ( "[" ++ formattedTime ++ "] " ++ "[SUCCESS] " ++ s ) 
    hSetSGR stdout [Reset]

-- |Prints an informative message /(prefixed with the '__[INFO]__' tag)/
printInfo :: String -> IO ()
printInfo s = do
    formattedTime <- getCurrentTimeFormatted
    hSetSGR stdout [SetColor Foreground Vivid Blue]
    hPutStrLn stdout ( "[" ++ formattedTime ++ "] " ++ "[INFO] " ++ s ) 
    hSetSGR stdout [Reset]

-- |Prints a regular message /(i.e with no prefix)/
printMsg :: String -> IO ()
printMsg s = do 
    formattedTime <- getCurrentTimeFormatted
    hPutStrLn stdout ( "[" ++ formattedTime ++ "] " ++ s)

-- |Class / wrapper for convenient use within another monad.
-- Comes complete with a default implementation
class (MonadIO m) => Logger m where
    -- |Logs an error message /(prefixed with the '__[ERROR]__' tag)/
    logError    :: String -> m ()
    logError    = liftIO . printError

    -- |Logs a warning message /(prefixed with the '__[WARNING]__' tag)/
    logWarning  :: String -> m ()
    logWarning  = liftIO . printWarning

    -- |Logs a success message /(prefixed with the '__[SUCCESS]__' tag)/
    logSuccess  :: String -> m ()
    logSuccess  = liftIO . printSuccess

    -- |Logs an informative message /(prefixed with the '__[INFO]__' tag)/
    logInfo     :: String -> m ()
    logInfo     = liftIO . printInfo

    -- |Logs a regular message /(i.e with no prefix)/
    logMsg      :: String -> m ()
    logMsg      = liftIO . printMsg

-- |Exposed logger capability within the IO monad
instance Logger IO where
    logError    = printError
    logWarning  = printWarning
    logSuccess  = printSuccess
    logInfo     = printInfo
    logMsg      = printMsg