module Gaia.Input.Input (
    Input(..)
    ) where

import Control.Monad.IO.Class

type InputID = String

-- Basic input binding that maps a key/action to a concrete action
type InputBinding a = (InputID, a)

--mkBinding :: 

--pump :: [InputBinding a] -> [String] -> [a]
--pump bindings input = buildInput filteredInput where
--    filteredInput = filter () input

class MonadIO m => Input m where
    isPressed       :: InputID -> m Bool
    isReleased      :: InputID -> m Bool
    getAxisValue    :: InputID -> m Float
    bindInput       :: InputBinding a -> m ()