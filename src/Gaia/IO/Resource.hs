module Gaia.IO.Resource (
    ResourceOpT (..),
    ResourceOpIO (..),
    ResourceLoader (..),
    
    loadResourceFromFile,
    loadResourceFromFile_,
    unloadResource,
    lookupResource,

    mkCache,
    evalResourceOp,
    runResourceOp,

    getResourceIdList,

    -- | = Generic loader(s)
    textLoader
    ) where

import Gaia.Debug.Log

import qualified Data.Map as M
import qualified Data.ByteString as B
import qualified Data.ByteString.UTF8 as U
import Control.Monad.Trans.State.Lazy
import Control.Monad.IO.Class
import System.Directory

-- |Resource id as a string (for now)
type ResourceId = String 

-- |A resource blob (bytestring for now)
type ResourceBlob = B.ByteString

-- |A resource cache
type ResourceCache a = M.Map ResourceId a

-- |A basic loader
data ResourceLoader a = ResourceLoader { load :: ResourceBlob -> a }

-- |A resource operator
type ResourceOpT r {-m-} {-a-} = StateT ( ResourceCache r ) {-m-} {-a-}

-- |Synonym for convenience
type ResourceOpIO r {-a-} = ResourceOpT r IO {-a-}

-- |Exposes logging capability within a resource operation
instance (MonadIO m) => Logger ( ResourceOpT r m )

-- |Makes an empty cache
mkCache :: ResourceCache a 
mkCache = M.empty 

-- |A resource operation /(e.g. can be loading or writing)/
evalResourceOp :: (Monad m) => ResourceOpT r m a -> ResourceCache r -> m a
evalResourceOp op cache = evalStateT op cache

-- |Runs a resource operation, returning a pair containing the result of the operation /(e.g. the resource)/ & the cache
runResourceOp :: (Monad m) => ResourceOpT r m a -> ResourceCache r -> m ( a, ResourceCache r )
runResourceOp op cache = runStateT op cache 

-- |Returns the list of resource ids stored in the given cache
getResourceIdList :: ResourceCache r -> [ResourceId]
getResourceIdList = M.keys

-- Loads from a file using a given loader
loadFromFile :: FilePath -> ResourceLoader a -> IO a 
loadFromFile path loader = do
    byteString <- B.readFile path
    return $ ( load loader ) byteString

-- |Loads a resource from a given file path & returns the resource /(if it could be loaded)/
loadResourceFromFile :: ResourceId -> FilePath -> ResourceLoader a -> ResourceOpIO a (Maybe a)--ResourceOpT a IO (Maybe a)
loadResourceFromFile rId path loader = do
    fileExists <- liftIO $ doesFileExist path
    case (fileExists) of
        False -> logError ( "File " ++ (show path) ++ " does not exist." ) >> return Nothing
        True  -> do 
            res <- liftIO $ loadFromFile path loader
            modify ( \s -> M.insert rId res s )
            return $ Just res

-- |Loads a resource from a given file path without returning a handle to the resource
loadResourceFromFile_ :: ResourceId -> FilePath -> ResourceLoader a -> ResourceOpIO a ()--ResourceOp a IO ()
loadResourceFromFile_ rId path loader = do
    res <- liftIO $ loadFromFile path loader
    modify ( \s -> M.insert rId res s )

-- |Looks up a resource & returns it if if already exists in the cache
lookupResource :: ResourceId -> ResourceOpIO a (Maybe a)--ResourceOp a IO (Maybe a)
lookupResource rId = gets ( M.lookup rId )

-- |Explicitely unloads the given resource by id
unloadResource :: ResourceId -> ResourceOpIO a ()--ResourceOp a IO ()
unloadResource rId = modify ( \s -> M.delete rId s )

-- |Simple resource loader that assumes a string
textLoader :: ResourceLoader String
textLoader = ResourceLoader $ U.toString