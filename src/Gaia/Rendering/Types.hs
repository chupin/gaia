module Gaia.Rendering.Types (
    Colour4(..), 
    Colour4f
    ) where

data Colour4 a = Colour4 a a a a deriving (Show)

type Colour4f  = Colour4 Float