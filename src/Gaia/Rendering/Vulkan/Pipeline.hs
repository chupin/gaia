module Gaia.Rendering.Vulkan.Pipeline (
    mkPipeline,
    ) where

mkPipeline :: Pipeline 
mkPipeline = Pipeline $ {
    initPipeline        = initPipelineVk,
    consumeRenderStage  = consumeRenderStageVk
}

initPipelineVk :: IO ()
initPipelineVk = error "Not implemented"

consumeRenderStageVk :: RenderStage -> IO ()
consumeRenderStageVk _ = error "Not implemented"