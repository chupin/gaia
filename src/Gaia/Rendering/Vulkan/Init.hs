{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE DataKinds #-}

module Gaia.Rendering.Vulkan.Init (
    createInstance
    ) where

import Gaia.Debug.Log

import Graphics.Vulkan
import Graphics.Vulkan.Core_1_0
import Foreign.C.String
import Foreign.Storable
import Foreign.Marshal.Alloc

data VulkanContext = VulkanContext {
    physicalDevice :: !VkPhysicalDevice
}

createInstance :: IO VkInstance
createInstance = 
    withCString "Test" $ \progNamePtr ->
    withCString "Gaia" $ \engineNamePtr -> do
        logInfo "Creating Vulkan instance..."

        -- VkApplicationInfo
        appInfo <- newVkData $ \appInfoPtr -> do
            writeField @"engineVersion"           appInfoPtr (_VK_MAKE_VERSION 1 0 0 )
            writeField @"pEngineName"             appInfoPtr engineNamePtr
            writeField @"applicationVersion"      appInfoPtr (_VK_MAKE_VERSION 1 0 0)
            writeField @"pApplicationName"        appInfoPtr progNamePtr
            writeField @"pNext"                   appInfoPtr VK_NULL_HANDLE
            writeField @"sType"                   appInfoPtr VK_STRUCTURE_TYPE_APPLICATION_INFO
            writeField @"apiVersion"              appInfoPtr (_VK_MAKE_VERSION 1 1 97)

        -- VkInstanceCreateInfo
        instCreateInfo <- newVkData $ \instCreateInfoPtr -> do
            writeField @"pApplicationInfo"        instCreateInfoPtr (unsafePtr appInfo)
            writeField @"ppEnabledExtensionNames" instCreateInfoPtr VK_NULL_HANDLE
            writeField @"enabledExtensionCount"   instCreateInfoPtr 0
            writeField @"ppEnabledLayerNames"     instCreateInfoPtr VK_NULL_HANDLE
            writeField @"enabledLayerCount"       instCreateInfoPtr 0
            writeField @"flags"                   instCreateInfoPtr 0
            writeField @"pNext"                   instCreateInfoPtr VK_NULL_HANDLE
            writeField @"sType"                   instCreateInfoPtr VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO

        -- Create the actual instance
        (vkResult, vkInstance) <- alloca $ \vkInstPtr -> do
            vkRes   <- vkCreateInstance (unsafePtr instCreateInfo) VK_NULL_HANDLE vkInstPtr
            vkI     <- peek vkInstPtr
            return (vkRes, vkI)

        -- Return the instance
        if vkResult == VK_SUCCESS 
            then logInfo "Vulkan instance created" >> return vkInstance 
            else error "Failed"