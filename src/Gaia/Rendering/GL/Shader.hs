module Gaia.Rendering.GL.Shader where

import Gaia.IO.Resource

import qualified Data.ByteString.UTF8 as U
import Data.List
import Data.Maybe

data ShaderSource = ShaderSource {
    source :: !String
} deriving (Show)

-- |Just an empty shader source
emptyShaderSource :: ShaderSource
emptyShaderSource = ShaderSource { source = "" }

-- |Loads a shader source resource (may contain multiple #includes)
shaderSourceLoader :: ResourceLoader ShaderSource
shaderSourceLoader = ResourceLoader $ ( \b -> 
    let content             = U.toString b
        includes            = extractIncludes content
        preprocessedSource  = preprocessSource content includes
    in ShaderSource { source = preprocessedSource } ) 

preprocessSource :: String -> [(Integer, String)] -> String
preprocessSource content _ = content -- For now just return the content

-- |Extracts the list of includes from the given source & returns a list of pairs containing the line at which the include was encountered & the path to the include
extractIncludes :: String -> [( Integer, String )]
extractIncludes xs = map fromJust ( filter isJust ( map extractIncludes' ( lines xs ) ) ) where 
    includeToken         = "#include"
    extractIncludes' xs' = if includeToken `isPrefixOf` xs' then Just( 0, drop ( length includeToken ) xs' ) else Nothing

-- |Finds the first occurence of the given list inside a second one list - returns a pair (if it exists) with the index in the second list at which the first list was found & the remainder of the second list 
findFirstOccurenceOf :: ( Eq a ) => [a] -> [a] -> Maybe ( Integer, [a] )
findFirstOccurenceOf _ []   = Nothing
findFirstOccurenceOf [] ys  = Just ( 0, ys )
findFirstOccurenceOf xs ys  = findFirstOccurenceOf' xs ys 0 where
    findFirstOccurenceOf' _  []  _ = Nothing
    findFirstOccurenceOf' xs ys' n = if xs `isPrefixOf` ys' then Just ( n, drop ( length xs ) ys' ) else findFirstOccurenceOf' xs ( drop 1 ys' ) ( n + 1 )

-- |Find all occurences of the given list inside a second list - returns a list of pairs containing the index in the second list at which the first list was found & the remainder of the second list
findAllOccurencesOf :: ( Eq a ) => [a] -> [a] -> [ ( Integer, [a] ) ]
findAllOccurencesOf xs ys = findAllOccurencesOf' xs ys 0 where
    findAllOccurencesOf' _ [] _    = []
    findAllOccurencesOf' xs ys' n0 = case findFirstOccurenceOf xs ys' of
                                        Nothing         -> []
                                        Just( n, ys'' ) -> ( n0 + n, ys'' ) : findAllOccurencesOf' xs ys'' ( n0 + n + ( fromIntegral ( length xs ) ) )