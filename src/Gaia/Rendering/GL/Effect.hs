module Gaia.Rendering.GL.Effect where

import Gaia.Debug.Log
import Gaia.Rendering.GL.Log

import Data.StateVar
import qualified Graphics.Rendering.OpenGL as GL
import qualified Data.ByteString.UTF8 as ByteString

{-# ANN createShaderProgram "HLint: ignore Use uncurry" #-}

-- Destroys the specified shader program
destroyShaderProgram :: GL.Program -> IO ()
destroyShaderProgram program = do 
    shaders <- get ( GL.attachedShaders program )
    GL.deleteObjectNames shaders
    GL.deleteObjectName program

-- Creates a shader program
createShaderProgram :: [( GL.ShaderType, String )] -> IO (Maybe GL.Program)
createShaderProgram xs = do 
    -- Start off by creating the shaders
    shaderPrograms <- mapM (\(shaderType, source) -> createShader shaderType source ) xs
    if Nothing `elem` shaderPrograms 
        then do 
            logError "Aborting shader program creation"
            return Nothing
        else do 
            -- Create our program
            program <- GL.createProgram

            -- Routine for attaching shaders
            let attachShader p x = case x of
                    Nothing       -> return ()
                    (Just shader) -> GL.attachShader p shader   

            -- Attempt to attach the shaders to our program
            mapM_ ( attachShader program ) shaderPrograms      

            -- Perform linking
            GL.linkProgram program

            -- Check status of the linking step
            linkResult <- get $ GL.linkStatus program
            if linkResult
                then do
                    GL.validateProgram program
                    validateResult <- get $ GL.validateStatus program
                    if validateResult
                        then do 
                            logGLErrors
                            return (Just program)
                        else do 
                            infoLog <- GL.programInfoLog program
                            logError $ "Program failed to validate - log : \n" ++ infoLog
                            return Nothing
                else do
                    infoLog <- GL.programInfoLog program
                    logError $ "Program failed to link - log : \n" ++ infoLog
                    return Nothing       

createShader :: GL.ShaderType -> String -> IO (Maybe GL.Shader)
createShader shaderType source = do 
    -- Create new shader
    shader <- GL.createShader shaderType

    -- Bind the source
    GL.shaderSourceBS shader $= ByteString.fromString source
    
    -- Compile & return the shader
    GL.compileShader shader

    -- Check status & return the shader accordingly
    compileResult <- get ( GL.compileStatus shader )
    if compileResult  
        then do 
            logGLErrors
            return ( Just shader )
        else do
            infoLog <- GL.shaderInfoLog shader
            logError $ "Shader failed to compile - log : \n" ++ infoLog
            logGLErrors
            return Nothing