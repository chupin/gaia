module Gaia.Rendering.GL.Log (
    GLLogger(..),
    ) where

import Gaia.Debug.Log ( Logger(..) )

import qualified Graphics.Rendering.OpenGL as GL

-- |Extension for the Logger class to allow for logging GL errors
class (Logger m) => GLLogger m where
    logGLErrors :: m ()
    logGLErrors = GL.get GL.errors >>= mapM_ ( logError . ("OpenGL error : "++) . show )

-- |Default instance for IO
instance GLLogger IO