module Gaia.Rendering.GL.Pipeline (
    mkPipeline, 
    ) where

import Gaia.Debug.Log
import Gaia.Rendering.GL.Log
import Gaia.Rendering.Types
import Gaia.Rendering.Token
import Gaia.Rendering.RenderStage
import Gaia.Rendering.Pipeline

import Data.StateVar
import Data.Int
import Data.Maybe
import Data.Bits
import GHC.Float
import Control.Monad
import qualified Graphics.Rendering.OpenGL as GL

-- |GL implementation
mkPipeline :: Pipeline
mkPipeline = Pipeline { 
    initPipeline       = initGLStates,
    consumeRenderStage = consumeRenderStageGL
}

-- |GL implementation
initGLStates :: IO ()
initGLStates = do
    logInfo "Initialising GL states"
    GL.depthFunc $= (Just GL.Lequal)

-- |GL implementation
consumeRenderStageGL :: RenderStage -> IO ()
consumeRenderStageGL r = do 
    consumePrologueGL ( prologue r )
    consumeEpilogueGL ( epilogue r )
    consumeStreamGL   ( stream   r )

-- |GL implementation
consumeStreamGL :: Stream -> IO ()
consumeStreamGL s = do 
    mapM_ consumeTokenGL s
    logGLErrors

-- |GL implementation
consumeTokenGL :: Token -> IO ()
consumeTokenGL (PToken _) = return ()--logWarning "consumeTokenGL (PToken _) => Not implemented" >> return () --printInfo "Program" 
consumeTokenGL (BToken d) = return ()--logWarning "consumeTokenGL (BToken d) => Not implemented" >> return () --printInfo "Buffer" 
consumeTokenGL (DToken d) = consumeDrawTokenGL d
consumeTokenGL _ = error "Not implemented"

consumeDrawTokenGL :: DrawToken -> IO ()
consumeDrawTokenGL DrawArray   = return ()--logWarning "consumeDrawTokenGL DrawArray => Not implemented" >> return ()
consumeDrawTokenGL DrawIndexed = return ()--logWarning "consumeDrawTokenGL DrawIndexed => Not implemented" >> return ()

-- |Consumes a prologue & clears the target as required for rendering
consumePrologueGL :: Prologue -> IO ()
consumePrologueGL p = do
    colourClearFlag     <- setupAndReturnClearFlag GL.ColorBuffer   ( clearColour  p ) (\(Colour4 r g b a) -> GL.clearColor $= (GL.Color4 r g b a))
    depthClearFlag      <- setupAndReturnClearFlag GL.DepthBuffer   ( clearDepth   p ) (\d -> GL.clearDepthf $= d)
    stencilClearFlag    <- setupAndReturnClearFlag GL.StencilBuffer ( clearStencil p ) (\s -> GL.clearStencil $= fromIntegral s)
    GL.clear $ catMaybes [colourClearFlag, depthClearFlag, stencilClearFlag]
    logGLErrors
    where
        setupAndReturnClearFlag flag mValue function = case mValue of 
            Nothing     -> return Nothing
            Just value  -> (function value) >> return (Just flag)

-- |Consumes an epilogue
consumeEpilogueGL :: Epilogue -> IO ()
consumeEpilogueGL e = do
    case ( viewport e ) of
        Nothing                     -> GL.viewport $= (GL.Position 0 0, GL.Size (fromIntegral outputW) (fromIntegral outputH)) where
            -- #TODO : Feed the size of the render target / output
            outputW = 800
            outputH = 600
        (Just (Viewport x y w h))   -> GL.viewport $= (GL.Position (fromIntegral x) (fromIntegral y), GL.Size (fromIntegral w) (fromIntegral h))
    logGLErrors
