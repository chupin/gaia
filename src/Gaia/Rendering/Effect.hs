module Gaia.Effect where

-- The various supported shader types
data ShaderType = VertexShader | FragmentShader deriving (Show)

data Effect = Effect

-- Store effects sequentially for now
type EffectCache = [Effect]