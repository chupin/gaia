module Gaia.Rendering.Renderer (
    Renderer(..)
    ) where

import Gaia.Rendering.RenderStage

import Control.Monad.IO.Class

class MonadIO m => Renderer m where
    consumeRenderStage :: RenderStage -> m ()
