module Gaia.Rendering.GL (
    mkPipeline,
    createShaderProgram,
    destroyShaderProgram
    )
where

import Gaia.Rendering.GL.Pipeline
import Gaia.Rendering.GL.Effect