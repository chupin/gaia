module Gaia.Rendering.Pipeline (
    Pipeline(..)
    ) where

import Gaia.Rendering.RenderStage

data Pipeline = Pipeline { 
    initPipeline        :: IO (),
    consumeRenderStage  :: RenderStage -> IO ()
}