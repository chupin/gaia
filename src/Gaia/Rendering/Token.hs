module Gaia.Rendering.Token (
    BufferToken(..),
    DrawToken(..),
    ProgramToken(..),
    TextureToken(..),
    DebugToken(..),
    Token(..),
    Stream,
    StreamWriter,
    pushToken,
    execStreamWriter
    ) where

import Control.Monad.Writer

data BufferToken = VertexBuffer | IndexBuffer| UniformBuffer deriving (Show)

data DrawToken = DrawArray | DrawIndexed deriving (Show)

data ProgramToken = Program deriving (Show)

data TextureToken = Texture deriving (Show)

data DebugToken = DebugMarkerBegin | DebugMarkerEnd deriving (Show)

data Token = PToken  ProgramToken |
             BToken  BufferToken  |
             TToken  TextureToken |
             DToken  DrawToken    |
             DbToken DebugToken deriving (Show) 

type Stream = [Token]

type StreamWriter m a = WriterT Stream m a

pushToken :: ( Monad m ) => Token -> StreamWriter m ()
pushToken t = writer ( (), [t] )

execStreamWriter :: ( Monad m ) => StreamWriter m () -> m Stream
execStreamWriter = execWriterT 