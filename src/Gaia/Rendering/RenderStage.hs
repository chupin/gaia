module Gaia.Rendering.RenderStage where

import Gaia.Rendering.Token
import Gaia.Rendering.Types

data RenderTarget = RenderTarget deriving (Show)

-- |A viewport, defined by its origin & dimensions
data Viewport  = Viewport Int Int Int Int deriving (Show)
 
-- |Epilogue to be executed at the end of a render stage
data Epilogue = Epilogue { 
    outputTarget    :: !(Maybe RenderTarget),
    viewport        :: !(Maybe Viewport)
} deriving (Show)

-- |Basic prologue to be executed at the beginning of a render stage
data Prologue = Prologue {
    clearColour  :: !(Maybe Colour4f),
    clearDepth   :: !(Maybe Float),
    clearStencil :: !(Maybe Int)
} deriving (Show)

-- |Basic render stage
data RenderStage = RenderStage { 
    name     :: !String,
    epilogue :: !Epilogue, 
    prologue :: !Prologue,
    stream   :: !Stream
} deriving (Show)

-- |Default render stage
defaultRenderStage :: RenderStage
defaultRenderStage = RenderStage {
    name        = "RenderStage",
    epilogue    = defaultEpilogue,
    prologue    = defaultPrologue,
    stream      = []
}

-- |Default epilogue
defaultEpilogue :: Epilogue
defaultEpilogue = Epilogue { 
    outputTarget  = Nothing,
    viewport      = Nothing
}

-- |Default prologue
defaultPrologue :: Prologue
defaultPrologue = Prologue { 
    clearColour  = Nothing, 
    clearDepth   = Nothing,
    clearStencil = Nothing
}

