#version 330

#include "../include/common.sh"
#include "../include/pbr/brdf.sh"

in vec2 vTexCoord0;

out vec4 oColour;

void main()
{
	float nDotV      = vTexCoord0.x;
	float roughness  = vTexCoord0.y;	
	uint sampleCount = 4096u;
	oColour 		 = vec4( brdfIntegration( nDotV, roughness, sampleCount ), 0.0, 1.0 );
}