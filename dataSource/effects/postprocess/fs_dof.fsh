#version 330

in vec2 vTexCoord0;

uniform sampler2D uColourTexture;
uniform sampler2D uDofTexture;

out vec4 oColour;

void main()
{
	// Fetch colour & dof
	vec3 colour  = texture( uColourTexture, vTexCoord0 ).xyz;
	vec4 dof     = texture( uDofTexture, vTexCoord0 ).xyzw;
	
	// Mix colours
    oColour 	 = vec4( mix( colour.xyz, dof.xyz, 0.0/*dof.w*/ ), 1.0 );
}