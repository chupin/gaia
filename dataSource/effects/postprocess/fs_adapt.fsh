#version 330

#include "../include/common.sh"
#include "../include/pbr/defines.sh"

in vec2 vTexCoord0;

uniform sampler2D uPrevLumTexture;
uniform sampler2D uCurrLumTexture;

out vec4 oColour;

void main()
{
    // Fetch luminance data & extract the scene luminance in cd/m^2
    vec2 curLum        = texture( uCurrLumTexture, vTexCoord0 ).xy;
    float sceneLum     = curLum.x * LUMINANCE_RCP_SCALER;
    float stdDeviation = sqrt( curLum.y );

    // Compute clamped target luminance
    float minLum       = max( sceneLum - stdDeviation, 0.0 );
    float maxLum       = sceneLum + stdDeviation;
    float tgtCameraLum = clamp( sceneLum, minLum, maxLum );

    // Fetch the previous camera luminance in cd/m^2
    float prevCameraLum = texture( uPrevLumTexture, vTexCoord0 ).x;

    // Compute adaptation rate
    float brightenSpeed   = 0.025;
    float darkenSpeed     = 0.025;
    float adaptationSpeed = tgtCameraLum > sceneLum ? darkenSpeed : brightenSpeed;

    // Adapt
    float adaptation      = 1.0 - exp( -adaptationSpeed * Context.deltaTime );
    float newCameraLum    = mix( prevCameraLum, tgtCameraLum, adaptation);

	// Write out info
    oColour = vec4( newCameraLum, 0.0, 0.0, 1.0 );
}