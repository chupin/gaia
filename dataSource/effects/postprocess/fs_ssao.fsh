#version 330

#include "../include/common.sh"

in vec2 vTexCoord0;

uniform sampler2D uNoiseTexture;
uniform sampler2D uDepthTexture;
uniform sampler2D uColourTexture;

out vec4 oColour;

// Inspired by : http://theorangeduck.com/page/pure-depth-ssao

vec3 normalFromDepth( in float depth, in vec2 texcoords) 
{  
  const vec2 offset1 = vec2(0.0,0.001);
  const vec2 offset2 = vec2(0.001,0.0);
  
  float depth1 = texture(uDepthTexture, texcoords + offset1).r;
  float depth2 = texture(uDepthTexture, texcoords + offset2).r;
  
  vec3 p1 = vec3(offset1, depth1 - depth);
  vec3 p2 = vec3(offset2, depth2 - depth);
  
  vec3 normal = cross(p1, p2);
  normal.z = -normal.z;
  
  return normalize(normal);
}

void main()
{
	const float totalStrength  	= 1.5;
  	const float base 		   	= 0.7;  
  	const float area 			= 0.09;
  	const float falloff 		= 0.000001;  
  	const float radius 	   		= 0.015;
  	const float noiseScale 		= 64.0;
  
  	const int samples = 16;
  	vec3 sampleSphere[samples] = vec3[](
      	vec3( 0.5381, 0.1856,-0.4319), vec3( 0.1379, 0.2486, 0.4430),
      	vec3( 0.3371, 0.5679,-0.0057), vec3(-0.6999,-0.0451,-0.0019),
     	vec3( 0.0689,-0.1598,-0.8547), vec3( 0.0560, 0.0069,-0.1843),
      	vec3(-0.0146, 0.1402, 0.0762), vec3( 0.0100,-0.1924,-0.0344),
      	vec3(-0.3577,-0.5301,-0.4358), vec3(-0.3169, 0.1063, 0.0158),
      	vec3( 0.0103,-0.5869, 0.0046), vec3(-0.0897,-0.4940, 0.3287),
      	vec3( 0.7119,-0.0154,-0.0918), vec3(-0.0533, 0.0596,-0.5411),
      	vec3( 0.0352,-0.0631, 0.5460), vec3(-0.4776, 0.2847,-0.0271)
  	);
  
  	float depth = texture(uDepthTexture, vTexCoord0).r;
	
	vec3 random = normalize( texture(uNoiseTexture, vTexCoord0 * ( noiseScale + depth * noiseScale ) ).rgb );  
 
  	vec3 position = vec3( vTexCoord0, depth );
  	vec3 normal   = normalFromDepth(depth, vTexCoord0);
  
  	float radiusDepth = radius/depth;
  	float occlusion   = 0.0;
	
  	for(int i=0; i < samples; i++) 
  	{  
    	vec3 ray 	  	= radiusDepth * reflect(sampleSphere[i], random);
		
    	vec3 hemiRay 	= position + sign( dot(ray,normal) ) * ray;
    
    	float occ_depth = texture( uDepthTexture, saturate(hemiRay.xy) ).r;
    	float difference = depth - occ_depth;
    
   		occlusion += step(falloff, difference) * (1.0 - smoothstep( falloff, area, difference) );
  	}
  
  	float ao = 1.0 - totalStrength * occlusion * (1.0 / samples);

  	// To separate texture
  	//oColour = vec4( saturate(ao + base), 0.0, 0.0, 1.0 );

  	// Pre-multiplied
  	vec4 colour = texture(uColourTexture, vTexCoord0);
  	oColour     = vec4( colour.rgb * saturate(ao + base), colour.a );
}