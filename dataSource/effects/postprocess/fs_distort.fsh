#version 330

in vec2 vTexCoord0;

uniform sampler2D tex0;
uniform sampler2D tex1;

out vec4 oColour;

void main()
{
	vec3 distortion = (texture( tex1, vTexCoord0 ).xyz * 2.0) - 1.0;
	vec4 colour     = texture( tex0, vTexCoord0 + distortion.xy * (1.0 - distortion.z) );
    oColour 	    = colour;
}