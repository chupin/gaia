#version 330

#include "../include/common.sh"

// FXAA implementation taken from : http://www.geeks3d.com/20110405/fxaa-fast-approximate-anti-aliasing-demo-glsl-opengl-test-radeon-geforce/3/
#define FXAA_REDUCE_MIN    (1.0/128.0)
#define FXAA_REDUCE_MUL    (1.0/8.0)
#define FXAA_SUBPIX_SHIFT  (1.0/4.0)
#define FXAA_SPAN_MAX      (8.0)

in vec2 vTexCoord0;

uniform sampler2D uColourTexture;

out vec4 oColour;

void main()
{
	vec2 size     	= textureSize( uColourTexture, 0 );
	vec2 uvOffset 	= ( 1.0 / size ) * ( 0.5 + FXAA_SUBPIX_SHIFT );

   vec3 rgbNW 		= texture2D( uColourTexture, vTexCoord0 + vec2( -1.0, -1.0 ) * uvOffset ).xyz;
   vec3 rgbNE 		= texture2D( uColourTexture, vTexCoord0 + vec2(  1.0, -1.0 ) * uvOffset ).xyz;
   vec3 rgbSW 		= texture2D( uColourTexture, vTexCoord0 + vec2( -1.0,  1.0 ) * uvOffset ).xyz;
   vec3 rgbSE 		= texture2D( uColourTexture, vTexCoord0 + vec2(  1.0,  1.0 ) * uvOffset ).xyz;
   vec3 rgbM  		= texture2D( uColourTexture, vTexCoord0 ).xyz;

   float lumaNW 	= calculateLuminance(rgbNW);
   float lumaNE 	= calculateLuminance(rgbNE);
   float lumaSW 	= calculateLuminance(rgbSW);
   float lumaSE 	= calculateLuminance(rgbSE);
   float lumaM  	= calculateLuminance(rgbM);

   float lumaMin 	= minM( lumaM, lumaNW, lumaNE, lumaSW, lumaSE );
   float lumaMax 	= maxM( lumaM, lumaNW, lumaNE, lumaSW, lumaSE );

   vec2 dir = vec2
   (
      -((lumaNW + lumaNE) - (lumaSW + lumaSE)),
       ((lumaNW + lumaSW) - (lumaNE + lumaSE))
   );

   float dirReduce = max((lumaNW + lumaNE + lumaSW + lumaSE) * (0.25 * FXAA_REDUCE_MUL),FXAA_REDUCE_MIN);
   float rcpDirMin = 1.0/(min(abs(dir.x), abs(dir.y)) + dirReduce);

   dir = min( vec2( FXAA_SPAN_MAX,  FXAA_SPAN_MAX ), max(vec2( -FXAA_SPAN_MAX, -FXAA_SPAN_MAX ),dir * rcpDirMin) ) * uvOffset;
   
   vec3 rgbA = 0.5 * (texture2D(uColourTexture, vTexCoord0.xy + dir * (1.0/3.0 - 0.5)).xyz + texture2D(uColourTexture, vTexCoord0.xy + dir * (2.0/3.0 - 0.5)).xyz);
   vec3 rgbB = rgbA * 0.5 + 0.25 * (texture2D(uColourTexture, vTexCoord0.xy + dir * -0.5).xyz + texture2D(uColourTexture,  vTexCoord0.xy + dir * 0.5).xyz);
   
   float lumaB = calculateLuminance(rgbB);
   
   if( (lumaB < lumaMin) || (lumaB > lumaMax) ) 
      oColour = vec4(rgbA, 1.0);
   else 
      oColour = vec4(rgbB, 1.0);
}