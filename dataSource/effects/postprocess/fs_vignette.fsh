#version 330

in vec2 vTexCoord0;

uniform sampler2D uColourTexture;
uniform sampler2D uVignetteTexture;

out vec4 oColour;

void main()
{
	float vignette 	= 1.0 - 0.25 * texture( uVignetteTexture, vTexCoord0 ).a;
	vec4 colour  	= texture( uColourTexture, vTexCoord0 );
    oColour 	 	= vec4( colour.xyz * vignette, colour.w );
}