#version 330

out vec4 oColour;

void main()
{
	vec2 depthBias       = vec2( 0.005f, 0.005f );
	float depth          = gl_FragCoord.z / gl_FragCoord.w;
	float depthSlopeBias = max( dFdx(depth), dFdy(depth) );
	gl_FragDepth 		 = max( depth + depthSlopeBias * depthBias.x + depthBias.y, -1.0 );
}