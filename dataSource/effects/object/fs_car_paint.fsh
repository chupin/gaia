#version 450

#define FEATURE_PREMULTIPLIED_ALPHA
#define FEATURE_SECONDARY_REFLECTIONS

#include "../include/pbr/lighting.sh"
#include "../include/shadow.sh"

in vec2 vTexCoord0;
in vec3 vNormal;
in vec3 vTangent;
in vec3 vBinormal;
in vec3 vWorldPos;

uniform sampler2D uAlbedoTexture;
uniform sampler2D uSurfaceTexture;
uniform sampler2D uNormalTexture;
uniform sampler2D uAOTexture;

out vec4 oColour;

void main()
{
	// Normal, tangent & binormal
	vec3 normal 			= normalize( vNormal );
	vec3 tangent 			= normalize( vTangent );
	vec3 binormal			= normalize( vBinormal );

	// Calculate common vectors
	vec3 eye 				= Context.viewPosition.xyz - vWorldPos;
	vec3 eyeVector 			= normalize( eye );

	vec2 texCoord0 			= vTexCoord0;

	// World pos
	vec3 worldPos 			= vWorldPos;

	// Fetch textures
	vec4 albedoMap 			= texture( uAlbedoTexture,  texCoord0 );
	vec3 surfaceMap			= texture( uSurfaceTexture, texCoord0 ).xyz;
	vec3 normalMap 			= texture( uNormalTexture,  texCoord0 * 500.0 ).xyz;
	float aoMap 			= texture( uAOTexture,		vTexCoord0 ).x;

	// Dot3 normal
	float weight 			= 0.25;
	vec3 tangentNormal      = normalize( ( normalMap * 2.0 - 1.0 ) * vec3( weight, weight, 1.0 - weight ) );
	vec3 worldNormal 		= normalize( tangentNormal.x * tangent + tangentNormal.y * binormal + tangentNormal.z * normal );

	// Shadow
	vec3 shadow 			= calculateShadow( worldPos, worldNormal );

	// Layered coat
	{
		MaterialParams params;
		params.albedo					= albedoMap.rgb;
		params.transparency     		= 0.0;
		params.ao 						= aoMap;
		params.shadow					= shadow;
		params.normal  					= worldNormal;
		params.pos 						= worldPos;
		params.eye 						= eyeVector;
		params.brdf.ior 				= 1.0 + surfaceMap.x;
		params.brdf.roughness 			= 0.3;
		params.brdf.metallic  			= 0.5;

#if defined(FEATURE_SECONDARY_REFLECTIONS)
		params.secondaryBRDF.ior 		= 1.45;
		params.secondaryBRDF.roughness 	= 0.0;
		params.secondaryBRDF.metallic   = 0.0;
		params.secondaryNormal 			= normal;
#endif

#if defined(FEATURE_DEBUGGING)
		//params.texCoord0 						= texCoord0;
#endif

    	oColour = calculateLighting( params );
	}    
}