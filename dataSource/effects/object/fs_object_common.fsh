#include "../include/pbr/lighting.sh"

#if defined(FEATURE_SHADOW)
#include "../include/shadow.sh"
#endif

#if defined(FEATURE_PARALLAX)
#include "../include/parallax.sh"
#endif

in vec2 vTexCoord0;
in vec3 vNormal;
in vec3 vTangent;
in vec3 vBinormal;
in vec3 vWorldPos;

uniform sampler2D uAlbedoTexture;
uniform sampler2D uSurfaceTexture;
uniform sampler2D uNormalTexture;

#if defined(FEATURE_AMBIENT_OCCLUSION)
uniform sampler2D uAOTexture;
#endif

#if defined(FEATURE_PARALLAX)
uniform sampler2D uHeightTexture;
#endif

out vec4 oColour;

void main()
{
	// Normal, tangent & binormal
	vec3 normal 			= normalize( vNormal );
	vec3 tangent 			= normalize( vTangent );
	vec3 binormal			= normalize( vBinormal );

	// Calculate common vectors
	vec3 eye 				= Context.viewPosition.xyz - vWorldPos;
	vec3 eyeVector 			= normalize( eye );

	// Parallax
#if defined(FEATURE_PARALLAX)
	vec2 parallaxOffset     = calculateParallaxOffset( vTexCoord0, eye, tangent, binormal, normal, uHeightTexture, vec4(1.0, 0.0, 0.0, 0.0) );
	vec2 texCoord0			= vTexCoord0 + parallaxOffset.xy;
#else
	vec2 texCoord0 			= vTexCoord0;
#endif

	// World pos
	vec3 worldPos 			= vWorldPos;

	// Fetch textures
	vec4 albedoMap 			= texture( uAlbedoTexture,  texCoord0 );
	vec3 surfaceMap			= texture( uSurfaceTexture, texCoord0 ).xyz;
	vec3 normalMap 			= texture( uNormalTexture,  texCoord0 ).xyz;

	// Parallax debug
/*#if defined(FEATURE_PARALLAX)
	if( int( texCoord0.x * 1000.0) % 100 == 0 || int( texCoord0.y * 1000.0) % 100 == 0 )
	{
		albedoMap.rgb = vec3(1.0, 0.0, 0.0);
	}
#endif*/

#if defined(FEATURE_AMBIENT_OCCLUSION)
	float aoMap 			= texture( uAOTexture,		texCoord0 ).x;
#else
	float aoMap 			= 1.0;
#endif

	// Dot3 normal
#if defined(FEATURE_NORMAL_MAP)
	vec3 tangentNormal      = normalize( normalMap * 2.0 - 1.0 );
	vec3 worldNormal 		= normalize( tangentNormal.x * tangent + tangentNormal.y * binormal + tangentNormal.z * normal );
#else
	vec3 worldNormal 		= normal;
#endif

	// Shadow
#if defined(FEATURE_SHADOW)
	vec3 shadow 			= calculateShadow( worldPos, worldNormal );
#else
	vec3 shadow 			= vec3(1.0);
#endif

	// Feed material properties
	MaterialParams params;
	params.albedo			= albedoMap.rgb;
	params.transparency     = 1.0 - albedoMap.a;
	params.ao 				= aoMap;
	params.shadow			= shadow;
	params.normal  			= worldNormal;
	params.pos 				= worldPos;
	params.eye 				= eyeVector;

	params.brdf.ior 		= 1.0 + surfaceMap.x;
	params.brdf.roughness 	= 1.0 - surfaceMap.y;
	params.brdf.metallic  	= surfaceMap.z;

#if defined(FEATURE_DEBUGGING)
	params.texCoord0 		= texCoord0;
#endif

    oColour = calculateLighting( params );
}