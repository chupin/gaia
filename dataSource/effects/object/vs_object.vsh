#version 450 core

#include "../include/common.sh"

//! Inputs
in vec4 aPosition;
in vec2 aTexCoord0;
in vec3 aNormal;
in vec3 aTangent;
in vec3 aBinormal;

//! Outputs
out vec2 vTexCoord0;
out vec3 vNormal;
out vec3 vTangent;
out vec3 vBinormal;
out vec3 vWorldPos;

void main()
{
	vec4 worldPosition     = Instance.worldMatrix * aPosition;    

	vWorldPos			   = worldPosition.xyz;

    vTexCoord0			   = aTexCoord0;
  
    vNormal                = ( Instance.worldMatrix * vec4( aNormal,   0.0 ) ).xyz;
    vTangent			   = ( Instance.worldMatrix * vec4( aTangent,  0.0 ) ).xyz;
    vBinormal			   = ( Instance.worldMatrix * vec4( aBinormal, 0.0 ) ).xyz;

    gl_Position 		   = Context.projectionViewMatrix * worldPosition;
}