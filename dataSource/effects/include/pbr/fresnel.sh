#ifndef PBR_FRESNEL
#define PBR_FRESNEL

float reflectanceAtNormalIncidence( in float ior )
{
	float f0 = ( 1.0 - ior ) / ( 1.0 + ior );
	return f0 * f0;
}

vec3 fresnelShlick( in vec3 v, in vec3 h, in vec3 f0 )
{
	float vDotH = saturatedDot( v, h );
	return f0 + (1.0 - f0) * pow( 1.0 - vDotH, 5.0 );
}

#endif