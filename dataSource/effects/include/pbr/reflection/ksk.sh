#ifndef PBR_KSK
#define PBR_KSK

#include "../../common.sh"
#include "../fresnel.sh"

#define MIN_ROUGHNESS 			(0.02)
#define MIN_ROUGHNESS_SQUARED 	(MIN_ROUGHNESS*MIN_ROUGHNESS)

float distributionGGX( in vec3 n, in vec3 h, in float r )
{
	float a         = max( r * r, MIN_ROUGHNESS_SQUARED );
	float a2 		= a * a;
	float nDotH    	= saturatedDot( n, h );
	float nDotH2   	= nDotH * nDotH;
	return a2 / ( PI * pow( nDotH2 * ( a2 - 1.0 ) + 1.0, 2.0 ) );
}

vec3 hemisphereSampleGGX( in vec2 uv, in float r )
{
	float a 			= max( r * r, MIN_ROUGHNESS_SQUARED );
	float a2  			= a * a;
	float phi			= uv.y * 2.0 * PI;
	float cosThetaSqr 	= ( 1.0 - uv.x ) / ( 1.0 + ( a2 - 1.0 ) * uv.x );
    float cosTheta 		= sqrt( cosThetaSqr );
    float sinTheta 		= sqrt( 1.0 - cosThetaSqr );
    return vec3(cos(phi) * sinTheta, sin(phi) * sinTheta, cosTheta);
}

// http://graphicrants.blogspot.co.uk/2013/08/specular-brdf-reference.html
float geometryGGXPartial( in vec3 h, in vec3 v, in float r )
{
	/*float a 			= max( r * r, MIN_ROUGHNESS_SQUARED );
	float a2 			= a * a;
	float cosTheta 		= dot( h, v );
	float cosThetaSqr   = cosTheta * cosTheta;
	return 2.0 * cosTheta / ( cosTheta + sqrt( a2 + ( 1.0 - a2 ) * cosThetaSqr ) );*/

    float a             = max( r * r, MIN_ROUGHNESS_SQUARED );
    float k             = a / 2.0;
    float cosTheta      = dot( h, v );
    return cosTheta / ( cosTheta * ( 1.0 - k ) + k );
}

float geometryGGX( in vec3 h, in vec3 v, in vec3 wi, in float r )
{
	return geometryGGXPartial( h, wi, r ) * geometryGGXPartial( h, v, r );
}

vec2 kskIntegrateBRDF( in float nDotV, in float roughness, in uint sampleCount )
{
	vec3 v;
	v.x = sqrt( 1.0 - nDotV * nDotV ); // sin
	v.y = 0;
	v.z = nDotV; 

	float A 		= 0.0;
	float B 		= 0.0;
	float samples 	= 0.0;

	for( uint i = 0u ; i < sampleCount ; ++i )
	{
		vec2 uv = hammersley2d( i, sampleCount );
		vec3 h  = hemisphereSampleGGX( uv, roughness );
		vec3 l  = 2.0 * dot( v, h ) * h - v;

		float nDotL = saturate( l.z );
		float nDotH = saturate( h.z );
		float vDotH = saturatedDot( v, h );

		if( nDotL > 0.0 )
		{
			// Fresnel
			float fB = pow( 1.0 - vDotH, 5.0 );
			float fA = 1.0 - fB;

			// Geometry
			float g  = geometryGGX( h, v, l, roughness );

			// Accumulate result
			A 		+= fA * g * nDotL;
			B 		+= fB * g * nDotL;
			samples += nDotL;
		}
	}

	return vec2( A, B ) / samples;
}

vec3 kskIntegrate( in samplerCube cubemap, in vec3 n, in vec2 uv, in float roughness, in uint sampleCount )
{
	vec2 rcpSize     = 1.0 / textureSize( cubemap, 0 );
	float rcpOmegaP	 = 1.0 / texelCoordSolidAngle( uv, rcpSize );

	mat3 frame   	 = generateFrame( n );
	vec4 radiance 	 = vec4(0.0);

	for( uint i = 0u ; i < sampleCount ; ++i )
	{
		vec2 r  		= hammersley2d( i, sampleCount );
		vec3 s  		= hemisphereSampleGGX( r, roughness );
		vec3 h          = normalize( frame * s );
		vec3 l			= 2.0 * saturatedDot( n, h ) * h - n;

		float hDotL 	= saturatedDot( h, l );
		
		if( hDotL > 0 )
		{
			float pdf       = distributionGGX( n, h, roughness ) / 4.0;
			float mipLevel  = selectMip( pdf, 0.0, sampleCount, rcpOmegaP );

			radiance.xyz   += textureLod( cubemap, l, mipLevel ).xyz * hDotL;
			radiance.w	   += hDotL;
		}
	}

	return radiance.xyz / radiance.w;
}

vec3 kskIndirect( in samplerCube Li, in sampler2D brdfLut, in vec3 n, in vec3 v, in vec3 f0, in float roughness, out vec3 kS )
{
	// Compute reflection vector
	vec3 r 		   = reflect( -v, n );

	// Pre-integrated BRDF
	float nDotV    = saturatedDot( n, v );
	vec2 envBRDF   = texture( brdfLut, vec2( nDotV, roughness ) ).xy;
	vec3 brdf      = f0 * envBRDF.x + envBRDF.y;

	// Pre-convolved specular
	float mipCount = 8.0;
	float lod      = roughness * ( mipCount - 1.0 );
	vec3 precalc   = textureLod( Li, r, lod ).xyz;

	// Use the BRDF term for energy conservation
	kS = brdf;

	return precalc * brdf;
}

vec3 kskDirectIsotropic( in vec3 Li, in vec3 wi, in vec3 n, in vec3 v, in vec3 f0, in float roughness, out vec3 kS )
{
	// Compute half vector
	vec3 h 				= normalize( v + wi );

	// Compute fresnel, distribution & geometry
	vec3 fresnel      	= fresnelShlick( v, h, f0 );
	float distribution 	= distributionGGX( n, h, roughness );
	float geometry      = geometryGGX( h, v, wi, roughness );

    // Compute dot product for normalisation
    float nDotV         = saturatedDot( n, v  );
    float nDotL         = saturatedDot( n, wi );

	// Assemble reflectance BRDF
	vec3 brdf 			= ( fresnel * distribution * geometry ) / ( 4.0 * nDotL * nDotV + 0.1 );

	// Use the BRDF term for energy conservation
	kS = brdf;

	return Li * brdf;
}

vec3 kskDirectAnisotropic( in vec3 Li, in vec3 wi, in vec3 n, in vec3 t, in vec3 b, in vec3 v, in vec3 f0, in float roughness, out vec3 kS )
{
	// Compute half vector
	vec3 h 				= normalize( v + wi );

	// Compute fresnel, distribution & geometry
	vec3 fresnel      	= fresnelShlick( v, h, f0 );
	float distribution 	= distributionGGX( n, h, roughness );
	float geometry      = geometryGGX( h, v, wi, roughness );

    // Compute dot product for normalisation
    float nDotV         = saturatedDot( n, v  );
    float nDotL         = saturatedDot( n, wi );

	// Assemble reflectance BRDF
	vec3 brdf 			= ( fresnel * distribution * geometry ) / ( 4.0 * nDotL * nDotV + 0.1 );

	// Use the BRDF term for energy conservation
	kS = brdf;

	return Li * brdf;
}

#define reflectionDirectIsotropic		kskDirectIsotropic
#define reflectionDirectAnisotropic		kskDirectAnisotropic
#define reflectionIndirect 				kskIndirect
#define reflectionIntegration			kskIntegrate
#define brdfIntegration					kskIntegrateBRDF

#endif
