#ifndef PBR_BRDF
#define PBR_BRDF

// Diffuse BRDF
#define DIFFUSION_LAMBERT (0)

#ifndef DIFFUSION
	#define DIFFUSION (DIFFUSION_LAMBERT)
#endif

#if (DIFFUSION == DIFFUSION_LAMBERT)
	#include "diffusion/lambert.sh"
#else
	#error No diffusion BRDF specified
#endif

// Reflection BRDF
#define REFLECTION_KSK (0)

#ifndef REFLECTION
	#define REFLECTION (REFLECTION_KSK)
#endif

#if (REFLECTION == REFLECTION_KSK)
	#include "reflection/ksk.sh"
#else
	#error No reflection BRDF specified
#endif

#endif
