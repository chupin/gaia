#ifndef PANORAMA_HEADER
#define PANORAMA_HEADER

#include "common.sh"

vec2 computePanoramicCoordinates( vec3 viewDir )
{
	vec2 uvCoords = vec2( 1.0 + atan( viewDir.x, -viewDir.z ) * ONE_OVER_PI, acos(viewDir.y) * ONE_OVER_PI );
	uvCoords.x 	  /= 2.0;
	return uvCoords;
}

vec3 computeSphericalCoordinates( vec2 uv )
{
	float theta = uv.x * TWO_PI;
	float phi   = uv.y * PI;
	return vec3( sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta) );
}

#endif