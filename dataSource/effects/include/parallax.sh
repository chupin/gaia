#ifndef PARALLAX
#define PARALLAX

#include "common.sh"

#define PARALLAX_MIN_SAMPLES (16u)
#define PARALLAX_MAX_SAMPLES (64u)
#define PARALLAX_SCALE       (0.01)

uniform float parallaxScale;

// Based off : http://www.gamedev.net/page/resources/_/technical/graphics-programming-and-theory/a-closer-look-at-parallax-occlusion-mapping-r3262
vec2 calculateParallaxOffset( in vec2 uv, in vec3 eye, in vec3 t, in vec3 b, in vec3 n, in sampler2D heightMap, in vec4 swizzle )
{
    // Build tangent to world matrix
    mat3 tangentToWorld = mat3( t, b, n );

    // Build world to tangent matrix & convert eye to tangent space
    mat3 worldToTangent = transpose( tangentToWorld );
    vec3 eyeTangent     = worldToTangent * eye;

    // Calculate limit
    float parallaxLimit = ( -length( eyeTangent.xy ) / eyeTangent.z ) * PARALLAX_SCALE;

    // Calculate offset vector
    vec2 offsetDir      = normalize( eyeTangent.xy ) * vec2(1.0, -1.0);
    vec2 maxOffset      = offsetDir * parallaxLimit;

    // Calculate number of samples
    uint noofSamples    = uint( mix( float(PARALLAX_MIN_SAMPLES), float(PARALLAX_MAX_SAMPLES), dot( normalize(eye), n ) ) );

    // Calculate step size
    float stepSize      = 1.0 / float(noofSamples);

    // Calculate partial derivatives
    vec2 dx             = dFdx( uv );
    vec2 dy             = dFdy( uv );

    // Initialise starting variables
    float currRayHeight = 1.0;
    float currHeight    = 1.0;
    float prevHeight    = 1.0;
    vec2 currOffset     = vec2( 0.0 );
    vec2 prevOffset     = vec2( 0.0 );

    // Sample height map until we find an intersection
    for( uint k = 0u ; k < noofSamples ; ++k )
    {
        currHeight = dot( swizzle, textureGrad( heightMap, uv + currOffset, dx, dy ) );

        if( currHeight > currRayHeight )
        {
            float delta1 = currHeight - currRayHeight;
            float delta2 = ( currRayHeight + stepSize ) - prevHeight;
            float ratio  = delta1 / ( delta1 + delta2 );
            currOffset   = ratio * prevOffset + ( 1.0 - ratio ) * currOffset;
            break;
        }
        else
        {                        
            prevHeight     = currHeight;
            prevOffset     = currOffset;
            currRayHeight -= stepSize;
            currOffset    += stepSize * maxOffset;
        }
    }

	return vec2( currOffset );
}

#endif