#ifndef COMMON_HEADER
#define COMMON_HEADER

#extension GL_EXT_texture_array : enable
#extension GL_ARB_fragment_layer_viewport : enable

#define PI 			(3.14159265359)
#define ONE_OVER_PI (1.0 / 3.14159265359)
#define TWO_PI      (2.0 * 3.14159265359)
#define FOUR_PI		(4.0 * 3.14159265359)
#define EPSILON 	(10e-5)

#define TEXTURE_CUBEMAP_POSITIVE_X (0)
#define TEXTURE_CUBEMAP_NEGATIVE_X (1)
#define TEXTURE_CUBEMAP_POSITIVE_Y (2)
#define TEXTURE_CUBEMAP_NEGATIVE_Y (3)
#define TEXTURE_CUBEMAP_POSITIVE_Z (4)
#define TEXTURE_CUBEMAP_NEGATIVE_Z (5)

layout(shared) uniform ubContext 
{
	mat4 	projectionViewMatrix;	
	mat4    inverseProjectionViewMatrix;
	mat4    inverseProjectionMatrix;
	mat4	projectionMatrix;
	mat4	viewMatrix;
	vec4	viewPosition;
	float   farDistance;
	float   nearDistance;
	float	time;
	float   deltaTime;
	float   cosTime;
} Context;

layout(shared) uniform ubInstance
{
	mat4 	worldMatrix;
} Instance;

layout(shared) uniform ubLighting
{
	vec3	sunDirection;
	float   padding;
	vec3    sunIlluminance;
} Lighting;

layout(shared) uniform ubPhysicalCamera
{
	float   exposure;
	float 	iso;
	float 	shutterSpeed;
	float   aperture;
	float   focalLength;
	float 	focalDistance;
} Camera;

vec3 saturate( in vec3 a )
{
	return clamp( a, 0.0, 1.0 );
}

vec2 saturate( in vec2 a )
{
	return clamp( a, 0.0, 1.0 );
}

float saturate( in float a )
{
	return clamp( a, 0.0, 1.0 );
}

float saturatedDot( in vec3 a, in vec3 b )
{
	return max( dot(a, b), 0.0 );
}

float calculateLuminance( in vec3 colour )
{
   vec3 luminanceCoefficient = vec3( 0.299, 0.587, 0.114 );
   return dot( colour, luminanceCoefficient );
}

float maxM( in float a, in float b, in float c )
{
	return max( a, max(b, c) );
}

float maxM( in float a, in float b, in float c, in float d )
{
	return max( a, maxM( b, c, d ) );
}

float maxM( in float a, in float b, in float c, in float d, in float e )
{
	return max( a, maxM( b, c, d, e ) );
}

float minM( in float a, in float b, in float c )
{
	return min( a, min(b, c) );
}

float minM( in float a, in float b, in float c, in float d )
{
	return min( a, minM( b, c, d ) );
}

float minM( in float a, in float b, in float c, in float d, in float e )
{
	return min( a, minM( b, c, d, e ) );
}

vec3 computeViewDir( in vec2 uv )
{
	vec4 clipPos = vec4( uv * 2.0 - 1.0, 1.0, 1.0 );
	vec4 viewPos = Context.inverseProjectionViewMatrix * clipPos;
	return normalize( ( viewPos.xyz / viewPos.w ) - Context.viewPosition.xyz );
}

vec3 computeViewDir( in vec2 uv, in int layer )
{
	vec3 up      = vec3(0.0);
	vec3 forward = vec3(0.0);

	switch( layer )
	{	
	case TEXTURE_CUBEMAP_POSITIVE_X:
		up 		= vec3(-1.0,0.0,0.0);
		forward = vec3(0.0,-1.0,0.0);
	break;

	case TEXTURE_CUBEMAP_NEGATIVE_X:
		up 		= vec3(1.0, 0.0,0.0);
		forward = vec3(0.0,-1.0,0.0);
	break;

	case TEXTURE_CUBEMAP_POSITIVE_Y:
		up 		= vec3(0.0,1.0,0.0);
		forward = vec3(0.0,0.0,1.0);
	break;

	case TEXTURE_CUBEMAP_NEGATIVE_Y:
		up 		= vec3(0.0,-1.0,0.0);
		forward = vec3(0.0,0.0,-1.0);
	break;

	case TEXTURE_CUBEMAP_POSITIVE_Z:
		up 		= vec3(0.0, 0.0,1.0);
		forward = vec3(0.0,-1.0,0.0);
	break;

	case TEXTURE_CUBEMAP_NEGATIVE_Z:
		up 		= vec3(0.0,0.0,-1.0);
		forward = vec3(0.0,-1.0,0.0);
	break;
	};

	vec3 right = cross( up, forward );
	mat3 frame = transpose( mat3( right, forward, up ) );

	vec3 clipPos = vec3( uv * 2.0 - 1.0, 1.0 );
	return normalize( frame * clipPos );
}

#endif