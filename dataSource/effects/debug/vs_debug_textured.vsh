#version 330

#include "../include/common.sh"

//! Inputs
in vec4 aPosition;
in vec2 aTexCoord0;
in vec4 aColour;

//! Outputs
out vec4 vColour;
out vec2 vTexCoord0;

void main()
{
    gl_Position = Context.projectionViewMatrix * aPosition;
    vTexCoord0	= aTexCoord0;
	vColour		= aColour;
}