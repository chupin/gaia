#version 330

in vec4 vColour;
in vec2 vTexCoord0;

uniform sampler2D tex0;

out vec4 oColour;

void main()
{
    oColour = vColour * texture( tex0, vTexCoord0 );
}