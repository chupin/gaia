#version 330

in vec4 vColour;

out vec4 oColour;

void main()
{
    oColour = vColour;
}