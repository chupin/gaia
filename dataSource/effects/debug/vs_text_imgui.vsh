#version 330

layout(std140) uniform ubViewportParameters
{
    vec2   viewportSize;
} Parameters;

//! Inputs
in vec2 aPosition;
in vec2 aTexCoord0;
in vec4 aColour;

//! Outputs
out vec4 vColour;
out vec2 vTexCoord0;

void main()
{
    mat4 transform = mat4
    (
        2.0 / Parameters.viewportSize.x, 0.0, 0.0, 0.0, 
        0.0, 2.0 / -Parameters.viewportSize.y, 0.0, 0.0, 
        0.0, 0.0, -1.0, 0.0, 
        -1.0, 1.0, 0.0, 1.0
    );

    gl_Position = transform * vec4( aPosition.xy, 0.0, 1.0 );
    vTexCoord0	= aTexCoord0;
	vColour		= aColour;
}