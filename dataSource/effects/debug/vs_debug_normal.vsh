#version 330

#include "../include/common.sh"

//! Inputs
in vec4 aPosition;
in vec3 aNormal;

//! Outputs
out vec3 vNormal;

void main()
{
    gl_Position 		   = Context.projectionViewMatrix * aPosition;
    vNormal                = aNormal;
}