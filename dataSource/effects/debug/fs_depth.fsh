#version 330

out vec4 oColour;

void main()
{
	gl_FragDepth 		 = max( (gl_FragCoord.z / gl_FragCoord.w), -1.0 );
}