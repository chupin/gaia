#version 330

#include "../include/common.sh"

//! Inputs
in vec4 aPosition;

void main()
{
    gl_Position = Context.projectionViewMatrix * aPosition;
}