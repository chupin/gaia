#version 330

//! Inputs
in vec4 aPosition;
in vec2 aTexCoord0;

//! Outputs
out vec2 vTexCoord0;

void main()
{
    gl_Position = aPosition;
    vTexCoord0	= aTexCoord0;
}