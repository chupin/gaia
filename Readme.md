# Readme

## Building the engine

*Gaia* builds using the standard legacy **cabal** build steps. It is recommend to build *gaia* using the **cabal v1-sandbox**, as follows :

```
#!/bin/bash
cabal v1-sandbox init # a no-op if the sandbox exists.
cabal v1-install --dependencies-only
cabal v1-build
```

## Generating/updating the documentation

The documentation for *Gaia* may be generated/updated using **haddock** and the following command :

```
#!/bin/bash
cabal haddock
```